import docx
from docx import Document
from FileSearch import getDocx
from pathlib import Path


class GetSuffix():
    def __init__(self, DocList, suffix):
        self.suf = suffix
        self.findSuffix(DocList)

    def findSuffix(self, DocList):
        self.files = []
        for doc in DocList:
            if doc.name[-len(self.suf):] == self.suf:
                self.files.append(doc)

    def __str__(self):
        return str(self.files)

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, i):
        return self.files[i]


def openDocx(fn: Path):
    with open(fn, 'rb') as f:
        document = Document(f)
    return document


if __name__ == "__main__":
    root = Path('ReportFiles')
    docx = getDocx(root)
    quants = GetSuffix(docx, 'QUANT.docx')
    maps = GetSuffix(docx, 'MAP.docx')
    eds = GetSuffix(docx, 'EDS.docx')
    d = openDocx(quants[0])
