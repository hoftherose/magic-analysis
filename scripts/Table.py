import docx
from docx import Document
from pathlib import Path

from FileSearch import getDocx
from ParseDocx import GetSuffix, openDocx


def getTable(doc: Document):
    for table in doc.tables:
        for row in table.rows:
            for cell in row.cells:
                for para in cell.paragraphs:
                    print(para.text)


if __name__ == "__main__":
    docx = getDocx(Path('ReportFiles'))
    quants = GetSuffix(docx, 'QUANT.docx')
    doc = openDocx(quants[0])
    getTable(doc)
