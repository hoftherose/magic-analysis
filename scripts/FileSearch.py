from pathlib import Path


def getDocx(root: Path):
    p = root.glob('**/*')
    return [x for x in p if x.is_file() and x.suffix == ".docx"]


if __name__ == "__main__":
    root = Path('ReportFiles')
    print(getDocx(root))
